const bcrypt = require('bcrypt');
const mongoose = require('mongoose');
const { isEmail } = require('validator');

let userSchema = new mongoose.Schema({
	name:{
		type: String,
		required: true,
		trim: true
	},
	lastname:{
		type: String,
		required: true,
		trim: true
	},
	email: {
		type: String,
		required: true,
		validate: [isEmail, 'invalid email'],
		createIndexes: { unique: true },
		lowercase: true,
		trim: true
	},
	password: {
		type: String,
		required: true,
		trim: true
	},
	deleted:{type: Boolean, default: false},
	refreshToken:{type: String}
})

userSchema.pre('save', async function (next) {
	const user = this;
	const hash = await bcrypt.hash(this.password, 10);

	this.password = hash;
	next();
})


let User = mongoose.model('User', userSchema);

exports.User = User;