const mongoose = require('mongoose');

let imageSchema = new mongoose.Schema({
  image:{
      _id: false,
      url: {type: String, trim: true}
    },
  desc:{
    type: String,
    trim: true
  },
  deleted:{type:Boolean, default: false}
})

let Image = mongoose.model('Gallery', imageSchema);

exports.Image = Image;