
const mongoose = require('mongoose');

let productSchema = new mongoose.Schema({
    type: {
        type: Number,
        required: true
    },
    title: {
        type: String,
        required: true,
        trim: true
    },
    code: {
        type: String,
        trim: true
    },
    published: {
        type: Boolean,
        required: true,
        default: false
    },
    brand: {
        type: String,
        required: true,
        trim: true
    },
    images: [
        {
            _id: false,
            url: { type: String, trim: true },
            isMain: { type: Boolean},
        }
    ],
    desc: {
        type: String,
        trim: true
    },
    info: [
        {
            type: {
                type: String,
            },
            text: {
                type: String,
            },
            tableHeaders: [String],
            tableData: [
                [
                    String
                ]
            ]

        }
    ],
    deleted: { type: Boolean }


});

let Product = mongoose.model('Product', productSchema);

exports.Product = Product;