const router = require('express').Router();
const galleryController = require('../controller/galleryController');
const {authenticateToken} = require('../middleware/user/UserAuth');

router.post('/gallery', authenticateToken, galleryController.saveImage)
.delete('/gallery/:id',authenticateToken, galleryController.deleteImage)
.get('/gallery',authenticateToken, galleryController.viewImages)
.put('/gallery/:id',authenticateToken, galleryController.updateImage)



module.exports = router