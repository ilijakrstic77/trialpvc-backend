const router = require('express').Router();
const productController = require('../controller/productContoller');
const {authenticateToken} = require('../middleware/user/UserAuth');

router.post('/product', authenticateToken, productController.createProduct)
    .get("/products",authenticateToken, productController.findProducts)
    .get("/products/:id", authenticateToken, productController.findProduct)
    .put("/products/:id", authenticateToken, productController.updateProduct)
    .delete("/products/:id", authenticateToken, productController.deleteProduct)
    .put("/products/:id/image", authenticateToken, productController.deleteImage)
    .delete("/products/:id/undelete", authenticateToken, productController.undeleteProduct)
    .get("/deleted/products", authenticateToken, productController.getDeletedProducts)

module.exports = router