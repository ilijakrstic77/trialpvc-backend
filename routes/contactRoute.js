const router = require('express').Router();
const contactController = require('../controller/contactController');

router.post('/contact', contactController.sendEmail)

module.exports = router