const router = require('express').Router();
const authController = require('../controller/authController');
const {authenticateToken} = require('../middleware/user/UserAuth');


router.post('/signup',  authController.saveUser)
  .post('/login', authController.authenticateUser)
  .put('/users/:id', authenticateToken, authController.updateUser)
  .get('/users', authenticateToken, authController.findUsers)
  .delete('/users/:id', authenticateToken, authController.deleteUser)
  .post('/token',  authController.verifyRefresh)
  .delete('/logout', authController.deleteToken)

module.exports = router
