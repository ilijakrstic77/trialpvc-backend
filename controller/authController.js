require('dotenv').config();
const { User } = require('../model/user');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

exports.saveUser = async (req, res) => {
	try {

		let name = req.body.name
		let lastname = req.body.lastname
		let email = req.body.email
		let password = req.body.password

		const find = await User.findOne({ email })
		if (find) return res.status(400).json({ message: "User with that email already exists" })
		const user = await User.create({ name, lastname, email, password });
		return res.status(200).json(user);

	} catch (error) {
		res.status(400).send()
	}
}
exports.authenticateUser = async (req, res) => {
	let email = req.body.email
	let password = req.body.password

	try {
		const user = await User.findOne({ email })
		if (!user) {
			return res.status(400).json({ message: "User not found" })
		}

		const validate = await bcrypt.compare(password, user.password);

		if (!validate) {
			return res.status(400).json({ message: "Wrong password" })
		}
		const accessToken = generateAccessToken(user);
		const refreshToken = jwt.sign({ user }, process.env.REFRESH_TOKEN_SECRET)
		await user.updateOne({ $set: { refreshToken } });
		console.log("Logged in");
		return res.status(200).json({ accessToken: accessToken, refreshToken: refreshToken, message: "Logged in successfully", status: 200 })
	} catch (err) {
		return res.status(400).json({ message: "Authenticating failed" })
	}
}

exports.verifyRefresh = async (req, res) => {
	const refreshToken = req.body.refreshToken
	if (refreshToken === null) return res.sendStatus(400)
	let find = await User.findOne({ refreshToken: refreshToken });
	if (!find) return res.sendStatus(400)
	jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, user) => {
		if (err) return res.sendStatus(400)
		const accessToken = generateAccessToken({ name: find.name, lastname: find.lastname, deleted: find.deleted, email: find.email, password: find.password })
		res.json({ accessToken: accessToken })
	})
}

exports.deleteToken = async (req, res) => {
	try {
		let user = await User.findOne({ refreshToken: req.body.refreshToken.refreshToken })
		if (req.body.refreshToken.refreshToken == user.refreshToken) {
			await user.updateOne({ $unset: { refreshToken: "" } });
			res.sendStatus(200).json({message: "Logged Out"});
		} else {
			res.status(400).json({ message: "Error while deleting token" })
		}
	} catch (error) {
		console.log(error)
	}
}

exports.updateUser = async (req, res) => {
	try {
		let { id } = req.params
		let name = req.body.name
		let lastname = req.body.lastname
		let email = req.body.email
		let password = await bcrypt.hash(req.body.password, 10)

		let user = await User.findByIdAndUpdate(id, {
			$set: {
				name,
				lastname,
				email,
				password
			}
		})
		return res.status(200).json({ message: "Updated" })
	} catch (error) {
		return res.status(400).json({ message: "Error while updating user" })
	}
}

exports.findUsers = async (req, res) => {
	try {
		let users = await User.find({ deleted: { $ne: true } }).select("name lastname email");
		return res.status(200).json({ users })
	} catch (error) {
		return res.status(400).json({ message: "Error while finding users" })
	}
}

exports.deleteUser = async (req, res) => {
	try {
		let { id } = req.params
		let user = await User.findByIdAndUpdate(id, { $set: { deleted: true } })
		return res.status(200).json({ message: "Success" })
	} catch (error) {
		return res.status(400).json({ message: "Error while deleting user" })
	}
}

function generateAccessToken(user) {
	return jwt.sign({ user }, process.env.ACCESS_TOKEN_SECRET, {expiresIn: '4h' })
}
