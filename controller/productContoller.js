const { Product } = require('../model/product');
const formidable = require('formidable');
const mkdirp = require('mkdirp');
const { v4: uuidv4 } = require('uuid');
const mongoose = require('mongoose');


exports.createProduct = async (req, res) => {
  try {
    let type, title, code, published, brand, images, desc, info

    let form = new formidable.IncomingForm();
    images = []

    form.parse(req)
    form.on('field', (field, value) => {

      switch (true) {
        case field === 'type':
          type = value
          break;
        case field === 'title':
          title = value
          break;
        case field === 'code':
          code = value
          break;
        case field === 'published':
          published = value
          break;
        case field === 'brand':
          brand = value
          break;
        case field === 'desc':
          desc = value
          break;
        case field === 'info[]':
          info = value;
          info = JSON.parse(info);
          break;
      }

    })

    form.on('fileBegin', (filename, file) => {
      let splited = file.name.split(".")
      let extension = splited[splited.length - 1]
      if (extension !== "png" && extension !== "jpg" && extension !== "jpeg") res.status(400).json({ message: "Wrong file type" })
      file.path = `static/images/products/${uuidv4()}.${extension}`;
      url = file.path;
      images = [...images, { url }]
      console.log(images)
    })

    form.on('end', async () => {
      await Product.create({ type, title, code, published, brand, images, desc, info });
      return res.status(200).json({ message: 'Created' });
    })

  } catch (error) {
    return res.status(400).json({ message: "Error while creating product" })
  }

}

exports.deleteProduct = async (req, res) => {
  try {
    let { id } = req.params;

    await Product.findByIdAndUpdate(id, { $set: { deleted: true, published: false } })


    return res.status(200).json({ message: "Success" })


  } catch (error) {
    return res.status(400).json({ message: "Error while deleting a product" })
  }
}

exports.getDeletedProducts = async(req,res) =>{
  const page = parseInt(req.query.page)
  const limit = parseInt(req.query.limit)

  const startIndex = (page - 1) * limit
  const endIndex = page * limit

  const products = {}
  let notDeletedProducts = await Product.find({ deleted: { $eq: true } }).countDocuments().exec()
  if (endIndex < notDeletedProducts) {
    products.next = {
      page: page + 1,
      limit: limit
    }
  }
  let numberOfPages = Math.ceil(await Product.countDocuments() / limit)
  products.pages = numberOfPages

  if (startIndex > 0) {
    products.previous = {
      page: page - 1,
      limit: limit
    }
  }
  
  try {
    products.products = await Product.find({ deleted: { $eq: true } })
      .limit(limit)
      .skip(startIndex)
      .sort({ "_id": -1 })
    return res.status(200).json({products})
  } catch (error) {
    return res.status(400).json({message: 'Error while finding deleted products'})
  }
}

exports.undeleteProduct = async (req, res) => {
  try {
    let { id } = req.params
    await Product.findByIdAndUpdate(id, { $set: { deleted: false } })
    return res.status(200).json({message: 'Success'})
  } catch (error) {
    return res.status(400).json({ message: "Error while undeleting a product" })
  }
}

exports.updateProduct = async (req, res) => {
  try {

    let { id } = req.params;
    let type, title, code, published, brand, images, desc, info
    // allowedFileExtension = ["", "jpeg", "png"]
    images = []

    let form = new formidable.IncomingForm();

    form.parse(req)

    form.on("field", (field, value) => {

      switch (true) {
        case field === 'type':
          type = value
          break;
        case field === 'title':
          title = value
          break;
        case field === 'code':
          code = value
          break;
        case field === 'published':
          published = value
          break;
        case field === 'brand':
          brand = value
          break;
        case field === 'desc':
          desc = value
          break;
        case field === 'info[]':
          info = value;
          info = JSON.parse(info);
          break;
      }

    })

    form.on('fileBegin', (filename, file) => {
      let splited = file.name.split(".")
      let extension = splited[splited.length - 1]
      if (extension !== "png" && extension !== "jpg" && extension !== "jpeg") res.status(400).json({ message: "Wrong file type" })
      file.path = `static/images/products/${uuidv4()}.${extension}`;
      url = file.path;
      images = [...images, { url }]
      console.log(images);
    })

    form.on('end', async () => {
      let dpProduct = await Product.findById(id, { images: 1 });

      images = [...dpProduct.images, ...images]

      for (let i = 1; i < images.length; i++) {
        const image = images;
        if (image.deleted == true) {

        }
      }
      await Product.findByIdAndUpdate(id, {
        $set: {
          type,
          title,
          code,
          published,
          brand,
          images,
          desc,
          info
        }
      })
      return res.status(200).json({ message: "Updated" });
    })

  } catch (error) {
    return res.status(400).json({ message: "Error while updating a product" })
  }
}

exports.findProduct = async (req, res) => {
  try {
    let { id } = req.params;
    let product = await Product.findById(id);
    return res.status(200).json({ product });
  } catch (error) {
    return res.status(400).json({ message: "Error while finding a product" })
  }
}
exports.deleteImage = async (req, res) => {
  try {
    let { id } = req.params
    let { url } = req.body
    const product = await Product.findById(id)
    await Product.findByIdAndUpdate(id, {
      $pull: {
        'images': { url }
      }
    })
    await product.save()
    return res.status(200).json({ message: "Image Deleted" })
  } catch (error) {
    return res.status(400).json({ error })
  }
}
exports.findProducts = async (req, res) => {
  const page = parseInt(req.query.page)
  const limit = parseInt(req.query.limit)

  const startIndex = (page - 1) * limit
  const endIndex = page * limit

  const products = {}
  let notDeletedProducts = await Product.find({ deleted: { $ne: true } }).countDocuments().exec()
  if (endIndex < notDeletedProducts) {
    products.next = {
      page: page + 1,
      limit: limit
    }
  }
  let numberOfPages = Math.ceil(await Product.countDocuments() / limit)
  products.pages = numberOfPages

  if (startIndex > 0) {
    products.previous = {
      page: page - 1,
      limit: limit
    }
  }
  try {
    products.products = await Product.find({ deleted: { $ne: true } })
      .limit(limit)
      .skip(startIndex)
      .sort({ "_id": -1 })
    return res.status(200).json({ products })
  } catch (error) {
    return res.status(400).json({ message: "Error while finding products" })
  }
}


