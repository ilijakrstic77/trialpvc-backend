const { Image } = require('../model/image');
const formidable = require('formidable');
const { v4: uuidv4 } = require('uuid');

exports.saveImage = async (req, res) => {
  try {
    let desc, image
    let form = new formidable.IncomingForm();

    form.parse(req)
    form.on('field', (field, value) => {
      switch (true) {
        case field === 'desc':
          desc = value;
          break;
      }
    })
    form.on('fileBegin', (filename, file) => {
      let splited = file.name.split(".")
      let extension = splited[splited.length - 1]
      if(extension !=="png" && extension!=="jpg" && extension !=="jpeg" ) res.status(400).json({message:"Wrong file type"})
      file.path = `static/images/gallery/${uuidv4()}.${extension}`
      url = file.path;
      image = { url };
    })
    form.on('end', async () => {
      let post = await Image.create({ image, desc })
      return res.status(200).json(post);
    })
  } catch (error) {
    return res.status(400).json({ message: "Error while uploading image to gallery" })
  }
}

exports.deleteImage = async (req, res) => {
  try {
    let { id } = req.params;
    let updated = await Image.findByIdAndUpdate(id, { $set: { deleted: true } })
    return res.status(200).json({ message: "Success" })

  } catch (error) {
    console.log(error)
    return res.status(400).json({ message: "Error while deleting an image" })
  }
}

exports.viewImages = async (req, res) => {

  const page = parseInt(req.query.page)
  const limit = parseInt(req.query.limit)

  const startIndex = (page - 1) * limit
  const endIndex = page * limit

  const images = {}
  let notDeletedImages = await Image.find({ deleted: { $ne: true } }).countDocuments().exec()

  if (endIndex < notDeletedImages) {
    images.next = {
      page: page + 1,
      limit: limit
    }
  }
  let numberOfPages = Math.ceil(await Image.countDocuments() / limit)
  images.pages = numberOfPages

  if (startIndex > 0) {
    images.previous = {
      page: page - 1,
      limit: limit
    }
  }

  try {
    images.images = await Image
    .find({ deleted: { $ne: true } })
    .limit(limit)
    .skip(startIndex)
    .sort({"_id": -1})
    .select("image desc");
    return res.status(200).json({ images })
  } catch (error) {
    return res.status(400).json({ message: "Error while finding images" })
  }
}

exports.updateImage = async (req, res) => {
  try {
    let { id } = req.params
    let desc
    let form = new formidable.IncomingForm();
    form.parse(req)
    form.on('field', (field, value) => {
      switch (true) {
        case field === 'desc':
          desc = value;
          break;
      }
    })
    form.on('fileBegin', (filename, file) => {
      let splited = file.name.split(".")
      let extension = splited[splited.length - 1]
      file.path = `static/images/gallery/${uuidv4()}.${extension}`
      url = file.path;
      image = { url };
    })
    form.on('end', async () => {
      let post = await Image.findByIdAndUpdate(id, { $set: { image, desc } })
      return res.status(200).json({message:"Image updated"});
    })
  } catch (error) {
    return res.status(400).json({ message: "Error while updating image in gallery" })
  }

}