let data = [
  {
    "id": 1,
    "type": "alu",
    "title": "N/A Series",
    "code": "N/A",
    "publish": false,
    "marka": "Salamander",
    "image": "assets/images/products/ESSENTIAL_WP3000.jpg",
    "images": [
      "assets/images/products/ESSENTIAL_WP3000.jpg"
    ],
    "desc": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took.",
    "info": [
      {
        "type": "text",
        "text": `It can work in harmony with fly swatter, blinds and shutters. It has world-class design that fits with
        domestic or foreign accessories.`
      },

      {
        "type": "text",
        "text": `Customizable with appropriate laminated and color options, you can create beautiful images to your place. It
        can give your windows a natural look with its wood-patterned laminated options and it can get modern looks
        with colors like anthracite gray.`
      },

      {
        "type": "text",
        "text": `It has 6 chambers and a width of 76 mm. It is produced with TPE seal and provides sealing with corner welding.
        It has perfect design with decorative and self-sealing laths. It supports many auxiliary profiles and is a
        functional structure that you can use in all your projects. It has a special slope to facilitate water
        discharge.`
      },
      {
        "type": "sub-title",
        "text": `Eco-Friendly Window`
      },
      {
        "type": "text",
        "text": `Maxwin PVC Window and Door Systems provide heat, sound and noise insulation, easy maintenance and long life
        advantages as well as being environmentally sensitive and 100% recyclable.`
      },
      {
        "type": "sub-title",
        "text": `Technicial Specifications`
      },
      {
        "type": "table",
        "headers": ["T1", "T2", "T3"],
        "data": [
          [
            "v1", "v2", "v3"
          ],
          [
            "v4", "v5", "v6"
          ]
        ]
      }
    ]
  },


  {
    "id": 2,
    "type": "pvc",
    "title": "N/A Series pvc",
    "code": "N/A",
    "publish": false,
    "marka": "Salamander",
    "image": "assets/images/products/ESSENTIAL_WP3000.jpg",
    "images": [
      "assets/images/products/ESSENTIAL_WP3000.jpg"
    ],
    "desc": "PVC Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took.",
    "info": [
      {
        "type": "text",
        "text": `It can work in harmony with fly swatter, blinds and shutters. It has world-class design that fits with
        domestic or foreign accessories.`
      },

      {
        "type": "text",
        "text": `Customizable with appropriate laminated and color options, you can create beautiful images to your place. It
        can give your windows a natural look with its wood-patterned laminated options and it can get modern looks
        with colors like anthracite gray.`
      },

      {
        "type": "text",
        "text": `It has 6 chambers and a width of 76 mm. It is produced with TPE seal and provides sealing with corner welding.
        It has perfect design with decorative and self-sealing laths. It supports many auxiliary profiles and is a
        functional structure that you can use in all your projects. It has a special slope to facilitate water
        discharge.`
      },
      {
        "type": "sub-title",
        "text": `Eco-Friendly Window`
      },
      {
        "type": "text",
        "text": `Maxwin PVC Window and Door Systems provide heat, sound and noise insulation, easy maintenance and long life
        advantages as well as being environmentally sensitive and 100% recyclable.`
      },
      {
        "type": "sub-title",
        "text": `Technicial Specifications`
      },
      {
        "type": "table",
        "headers": ["T1", "T2", "T3", "T4"],
        "data": [
          [
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an un", "v2", "v3", "v4"
          ],
          [
            "v4", "v5", "v6", "v7"
          ]
        ]
      }
    ]
  },

  //====================================

  {
    "id": 3,
    "type": "pvc",
    "title": "Streamline 76 AD",
    "code": "N/A",
    "publish": false,
    "marka": "Salamander",
    "image": "assets/images/products/ESSENTIAL_WP3000.jpg",
    "images": [
      "assets/images/products/ESSENTIAL_WP3000.jpg"
    ],
    "desc": `Ekstrudiran od tvrdog PVC-a,
    povrsina rama i krila u dve ravni "smaknuto", blago zaobljenih ivica, 5-komora
    termoizolacija profila Uf=1.4 W/qmK
    energoštedljiv-energo razred B,
    otporan na štetne uticaje atmosfere,
    ekološki-bez dodatka olova,
    dodatni profili: proširenje spojnica, okapnica i slično,
    alpsko bela boja i boje-teksture drveta,
    Termoizolacija prozora
    Uw=0.99W/qmK`,
    "info": [
      {
        "type": "sub-title",
        "text": `STAKLO`
      },

      {
        "type": "text",
        "text": `Standardna termoizolacija 2-slojnog LOW-E izostaklo širine 24mm, Ug=1.1W/qmK.
        Zahtevana termoizolacija energo razred B, 3-slojnog LOW-E izostaklo širine 36mm, Ug=0.6W/qmK.
        Visoko zahtevama termoizolacija energo razred A, širina stakla do 40mm, Ug=0.5W/qmK`
      },
      {
        "type": "sub-title",
        "text": `DIHTUNG`
      },
      {
        "type": "text",
        "text": `Dvostruki dihtung specijalne konstrukcije "trajno elastičan"`
      },
      {
        "type": "sub-title",
        "text": `DUBINA UGRADNJE`
      },
      {
        "type": "text",
        "text": `Širina profila 76mm pogodan za prvu ugradnju i restauraciju`
      },
      {
        "type": "sub-title",
        "text": `OJAČANJE`
      },
      {
        "type": "text",
        "text": `Ram i krilo ojačani prema potrebama statike, čeličnim pocinkovanim profilom debljine 1.5mm`
      },
    ]
  },
  {
    "id": 4,
    "type": "pvc",
    "title": "Streamline 76 AD",
    "code": "N/A",
    "publish": true,
    "marka": "Salamander",
    "image": "assets/images/products/ESSENTIAL_WP3000.jpg",
    "images": [
      "assets/images/products/ESSENTIAL_WP3000.jpg"
    ],
    "desc": `Ekonomičnost, Postojanost boja, Energoštedljiv-energo razred B,
    Stabilnost, Lako održavanje, Protivprovalna zaštita, Provetravanje (Air-Controller), Najbolji odnos kvaliteta i cena`,
    "info": [
      {
        "type": "sub-title",
        "text": `OSLONAC`
      },

      {
        "type": "text",
        "text": `Dubina oslonca stakla 20mm. Širina oslonca krina 12mm`
      },
      {
        "type": "sub-title",
        "text": `DEBLJINA ZIDA PROFILA`
      },
      {
        "type": "text",
        "text": `Prednji-zadnji zid profila klase B prema RAL-GZ 716/1, Teil 1 und 7`
      },
      {
        "type": "text",
        "text": `Standardna termoizolacija 2-slojnog LOW-E izostaklo širine 24mm, Ug=1.1W/qmK.
        Zahtevana termoizolacija energo razred B, 3-slojnog LOW-E izostaklo širine 36mm, Ug=0.6W/qmK.
        Visoko zahtevama termoizolacija energo razred A, širina stakla do 40mm, Ug=0.5W/qmK`
      },
      {
        "type": "sub-title",
        "text": `DIHTUNG`
      },
      {
        "type": "text",
        "text": `Dvostruki dihtung specijalne konstrukcije "trajno elastičan"`
      },
      {
        "type": "sub-title",
        "text": `DUBINA UGRADNJE`
      },
      {
        "type": "text",
        "text": `Širina profila 76mm pogodan za prvu ugradnju i restauraciju`
      },
      {
        "type": "sub-title",
        "text": `OJAČANJE`
      },
      {
        "type": "text",
        "text": `Ram i krilo ojačani prema potrebama statike, čeličnim pocinkovanim profilom debljine 1.5mm`
      },
      {
        "type": "sub-title",
        "text": `DIHTUNZI`
      },
      {
        "type": "text",
        "text": `Ram-krilo-staklo lajsna "trajno elastični" od TPE`
      },
      {
        "type": "sub-title",
        "text": `OJAČANJE`
      },
      {
        "type": "text",
        "text": `Pocinkovani čelični profil prema RAL-RG 716/1, Teil 1 und 7`
      },
      {
        "type": "sub-title",
        "text": `DRENAŽA I VENTILACIJA`
      },
      {
        "type": "text",
        "text": `Prednje komore na ramu i krilu profila za drenažu, odvod vode i ventilaciju`
      },
      {
        "type": "sub-title",
        "text": `ZASTAKLIVANJE`
      },
      {
        "type": "text",
        "text": `Standardno i po zahtevu izolaciono LOW-E staklo debljine 24-40mm`
      },
      {
        "type": "sub-title",
        "text": `OKOV`
      },
      {
        "type": "text",
        "text": `SIEGENIA okov širine 16mm. Evonut 13mm`
      },
      {
        "type": "sub-title",
        "text": `EnEV propis o uštedi energije - Energetska efikasnost`
      },
      {
        "type": "table",
        "headers": ["Predmet termoizolacije", "Standardna termoizolacija", "Zahtevna termoizolacija energo razred B", "Visoko zahtevna termoizolacija energo razred A"],
        "data": [
          [
            "Staklo", "Ug=1.1 W/qmK", "Ug=0.6 W/qmK", "Ug=0.5 W/qmK"
          ],
          [
            "Ram", "Uf= 1.4 W/qmK", "Uf= 1.4 W/qmK", "Uf= 1.4 W/qmK"
          ],
          [
            "Prozor (ALU ram stakla) PSI=0.06", "Uw=1.3W/qmK", "Uw=1.09W/qmK", "Uf= 0.94 W/qmK"
          ],
          [
            `Prozor ("Topli ram" TGI stakla) PSI=0.04`, "Uw=1.2W/qmK", "Uw=0.99W/qmK", "Uf= 0.89 W/qmK"
          ]

        ]
      },
      {
        "type": "sub-title",
        "text": `Rezultati inicijalnog testa ispitivanja prozora`
      },
      {
        "type": "table",
        "headers": ["Kriterijumi", "Osobine uskladjene prema tehničkim propisima", "Napomena"],
        "data": [
          [
            "Karakteristike prema RAL_RG 716/1", "Vazdušna propustljivost prema EN 12207 klasa 4, Vodoneporpusnost prema EN 12208 klase 9A, Otpor na udare vetra prema EN 12210 klase C4/B4, Otpor na radno opterećenje prema EN 13115 klase 1, Otpor na mehaničko opterećenje prema EN 12400 klasa 4", "Ispitano sa različitim kombinacijama rama i krila"
          ],
          [
            "Karakteristike prozora sa sistemom provetravanja", "Vazdušna propustljivost prema EN 12207 klasa 4, Vodoneporpustivost prema EN 12208 klase 9A", "Prozori sa Air Controller-om"
          ],
          [
            "Zvučna izolacija", "VDI 2719 SSK klasa 4   35db do 39db", "Ispitano sa različitim kombinacijama rama, krila i stakala"
          ],
          [
            `Zvučna izolacija sa sistemom za provetravanja`, "VDI 2719 SSK klasa 4    35db do 37db", "Prozori sa Air Controller-om"
          ]
          ,
          [
            `Protiv provalna zaštita`, "DIN V ENV 1627 klasa 2", "Ispitano sa različitim kombinacijama rama krila i okov SIEGENIA"
          ]
          ,
          [
            `Termoizolacija`, "Koeficijenta prolaza toplote-termoizolacija Uf-vrednosti 1.4(W/qmK) zavisno od prozorskog sistema AD", "Ispitano sa različitim kombinacijama rama i krila"
          ]

        ]
      }
    ]
  },
  {
    "id": 5,
    "type": "alu",
    "title": "AS 50 - Asistal (Hladni aluminijum)",
    "code": "N/A",
    "publish": true,
    "marka": "Asistal",
    "image": "assets/images/products/as50.jpg",
    "images": [
      "assets/images/products/as50.jpg"
    ],
    "desc": `Maksimalna vazdušna i vodena nepropustljivost zahvaljujući EPDM staklu I sistemu zatvaranja, Upotreba uglova za presovanje I pin konektora kompatibilnih sa drugim sistemima, Laka instalacija `,
    "info": [
      {
        "type": "table",
        "headers": ["Širina stoka", "Širina krila", "Debljina zida", "Debljina stakala"],
        "data": [
          [
            "50mm (opcija ovalnog i četvrtastog)", "58mm", "1.4mm", "24mm"
          ]
        ]
      },
      {
        "type": "sub-title",
        "text": `IZBOR DODATAKA`
      },
      {
        "type": "text",
        "text": `Profili po Evropskim standardima, Profil kompatibilan sa PVC dodacima`
      },
      {
        "type": "sub-title",
        "text": `TIPOVI OTVARANJA`
      },
      {
        "type": "text",
        "text": `Jednokrilni prozor, dvokrilni prozor, jednokrilna vrata unutrašnje otvaranje, dvokrilna vrata unutrašnje otvaranje, jednokrilna vrata spoljno otvaranje, dvokrilna vrata spoljno otvaranje`
      }
    ]
  },
  {
    "id": 6,
    "type": "alu",
    "title": "TE62 - ASISTAL (aluminijum sa termo prekidom)",
    "code": "N/A",
    "publish": true,
    "marka": "ASISTAL",
    "image": "assets/images/products/te62.png",
    "images": [
      "assets/images/products/te62.png"
    ],
    "desc": `Maksimalna vazdušna i vodena nepropustljivost zahvaljujuci EPDM staklu I sistemu zatvaranja, Upotreba uglova za presovanje I pin konektora kompatibilnih sa drugim sistemima, Laka instalacija `,
    "info": [
      {
        "type": "text",
        "text": `-Maksimalna vazdusna i vodena  nepropustljivost zahvaljujuci EPDM staklu I sistemu zatvaranja`
      },
      {
        "type": "text",
        "text": `-Upotreba uglova za presovanje I pin konektora kompatibilnih sa drugim sistemima`
      },
      {
        "type": "text",
        "text": `-Laka instalacija`
      },
      {
        "type": "sub-title",
        "text": `TEHNICKE SPECIFIKACIJE`
      },

      {
        "type": "table",
        "headers": ["Naziv", "Vrednost"],
        "data": [
          [
            "SIRINA STOKA", "62 mm"
          ],
          [
            "IRINA KRILA", "65 mm"
          ],
          [
            "DEBLJINA ZIDA", "1,3 mm"
          ],
          [
            "IZOLACISKA BARIJERA", "24 mm"
          ],
          [
            "DEBLJINA STAKLA", "10 – 45 mm"
          ],
          [
            "IZBOR DODATAKA", "Profili po Evropskim standardima. Profil kompatibilan sa PVC dodacima"
          ],
          [
            "TIPOVI OTVARANJA", `Jednokrilni prozor ,dvokrilni prozor, jednokrilna vrata unutrasnje otvaranje, dvokrilna vrata unutrasnje otvaranje, jednokrilna vrata spoljnootvaranje, dvokrilna vrata spoljno otvaranje, vrata na sklapanje,,klizni sistem`
          ]
        ]
      }
    ]
  },
  {
    "id": 7,
    "type": "pvc",
    "title": "WINDIAMOND",
    "code": "N/A",
    "publish": true,
    "marka": "WINDIAMOND",
    "image": "assets/images/products/wind.png",
    "images": [
      "assets/images/products/wind.png"
    ],
    "desc": `Maksimalna vazdušna i vodena nepropustljivost zahvaljujuci EPDM staklu I sistemu zatvaranja, Upotreba uglova za presovanje I pin konektora kompatibilnih sa drugim sistemima, Laka instalacija `,
    "info": [
      {
        "type": "text",
        "text": `WINDIAMOND je proizvod sa visokom termalnom I zvucnom izolacijom, sastoji se od  6 komora ima sirinu od 76mm,proziveden od TPE granula.  Savremenog dizajna  sa dekorativnim krilom.Tehnicke karakteristike , njegova struktura je takva da ga mozete koristiti u svim vasim porjektima. Dizajniran je u vise boja , tako da osim bele standardne boje mozete dobiti prozor sa  sarom boje drveta ili modernu antracit boju.  WinDiamond system prozora I vrata pruza toplotnu , zvucnu izolaciju , lak za odrzavanje sa dugim zivotnim vekom , takodje su ekoloski osetljivi I 100% reciklirajuci.
        `
      },
     
      {
        "type": "sub-title",
        "text": `TEHNICKE SPECIFIKACIJE`
      },

      {
        "type": "table",
        "headers": ["Naziv", "Vrednost"],
        "data": [
          [
            "Sirina profila", "76 mm"
          ],
          [
            "Broj komora", "6"
          ],
          [
            "Broj guma", "2"
          ],
          [
            "Tip gume TPE", "TPE Gray / Black"
          ],
          [
            "Debljina stakla", "24 mm"
          ],
        ]
      }
    ]
  }
]

// ---------------------------------------------------

// [
//   {
//      "type":"1",
//      "tableHeaders":[
//         "Širina stoka",
//         "Širina krila",
//         "Debljina zida",
//         "Debljina stakala"
//      ],
//      "tableData":[
//         [
//            "50mm (opcija ovalnog i četvrtastog)",
//            "58mm",
//            "1.4mm",
//            "24mm"
//         ]
//      ]
//   },
//   {
//      "type":"2",
//      "text":"`IZBOR DODATAKA`"
//   },
//   {
//      "type":"1",
//      "text":"`Profili po Evropskim standardima, Profil kompatibilan sa PVC dodacima`"
//   },
//   {
//      "type":"2",
//      "text":"`TIPOVI OTVARANJA`"
//   },
//   {
//      "type":"1",
//      "text":"`Jednokrilni prozor, dvokrilni prozor, jednokrilna vrata unutrašnje otvaranje, dvokrilna vrata unutrašnje otvaranje, jednokrilna vrata spoljno otvaranje, dvokrilna vrata spoljno otvaranje`"
//   }
// ]

// ----------------------------------------------------------------



export default data;
