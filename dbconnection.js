const mongoose = require('mongoose');
module.exports = () => {
    mongoose.connect('mongodb://localhost/trioalpvc', { useNewUrlParser: true, useFindAndModify: false, useUnifiedTopology: true })
    mongoose.set('useCreateIndex', true);
    mongoose.connection.once('open', () => console.log("Connected to MongoDB")).on('error', (err) => console.log('Connecton Error : ' + err));
}