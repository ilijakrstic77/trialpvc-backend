const mongoose = require ('mongoose');
const express = require('express');
const bodyParser = require('body-parser');
const mkdirp = require('mkdirp');
const cors = require('cors');
const dbconnection = require('./dbconnection');


mkdirp.sync("./static/images/products")
mkdirp.sync("./static/images/gallery")

const app = express();


app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(cors());

const productsRoute = require('./routes/productsRoute');
const userRoute = require('./routes/userRoute');
const contactRoute = require('./routes/contactRoute');
const galleryRoute = require('./routes/galleryRoute');

app.use('/static', express.static(__dirname + "/static"))
app.use('/api/v1', productsRoute);
app.use('/api/v1', userRoute);
app.use('/api/v1', contactRoute);
app.use('/api/v1', galleryRoute);

dbconnection();

app.listen(3000, ()=> console.log("Listening on port 3000"));
